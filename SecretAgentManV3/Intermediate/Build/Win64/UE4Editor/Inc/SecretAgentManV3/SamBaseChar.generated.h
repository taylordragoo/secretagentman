// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SECRETAGENTMANV3_SamBaseChar_generated_h
#error "SamBaseChar.generated.h already included, missing '#pragma once' in SamBaseChar.h"
#endif
#define SECRETAGENTMANV3_SamBaseChar_generated_h

#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_SPARSE_DATA
#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_RPC_WRAPPERS
#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASamBaseChar(); \
	friend struct Z_Construct_UClass_ASamBaseChar_Statics; \
public: \
	DECLARE_CLASS(ASamBaseChar, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SecretAgentManV3"), NO_API) \
	DECLARE_SERIALIZER(ASamBaseChar)


#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASamBaseChar(); \
	friend struct Z_Construct_UClass_ASamBaseChar_Statics; \
public: \
	DECLARE_CLASS(ASamBaseChar, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SecretAgentManV3"), NO_API) \
	DECLARE_SERIALIZER(ASamBaseChar)


#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASamBaseChar(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASamBaseChar) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASamBaseChar); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASamBaseChar); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASamBaseChar(ASamBaseChar&&); \
	NO_API ASamBaseChar(const ASamBaseChar&); \
public:


#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASamBaseChar(ASamBaseChar&&); \
	NO_API ASamBaseChar(const ASamBaseChar&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASamBaseChar); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASamBaseChar); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASamBaseChar)


#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_PRIVATE_PROPERTY_OFFSET
#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_9_PROLOG
#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_PRIVATE_PROPERTY_OFFSET \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_SPARSE_DATA \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_RPC_WRAPPERS \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_INCLASS \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_PRIVATE_PROPERTY_OFFSET \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_SPARSE_DATA \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_INCLASS_NO_PURE_DECLS \
	SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SECRETAGENTMANV3_API UClass* StaticClass<class ASamBaseChar>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SecretAgentManV3_Source_SecretAgentManV3_SamBaseChar_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

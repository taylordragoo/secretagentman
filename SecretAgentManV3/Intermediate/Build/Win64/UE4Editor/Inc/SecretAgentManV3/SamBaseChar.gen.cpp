// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SecretAgentManV3/SamBaseChar.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSamBaseChar() {}
// Cross Module References
	SECRETAGENTMANV3_API UClass* Z_Construct_UClass_ASamBaseChar_NoRegister();
	SECRETAGENTMANV3_API UClass* Z_Construct_UClass_ASamBaseChar();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_SecretAgentManV3();
// End Cross Module References
	void ASamBaseChar::StaticRegisterNativesASamBaseChar()
	{
	}
	UClass* Z_Construct_UClass_ASamBaseChar_NoRegister()
	{
		return ASamBaseChar::StaticClass();
	}
	struct Z_Construct_UClass_ASamBaseChar_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASamBaseChar_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_SecretAgentManV3,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASamBaseChar_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "SamBaseChar.h" },
		{ "ModuleRelativePath", "SamBaseChar.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASamBaseChar_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASamBaseChar>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASamBaseChar_Statics::ClassParams = {
		&ASamBaseChar::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASamBaseChar_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASamBaseChar_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASamBaseChar()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASamBaseChar_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASamBaseChar, 1510452532);
	template<> SECRETAGENTMANV3_API UClass* StaticClass<ASamBaseChar>()
	{
		return ASamBaseChar::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASamBaseChar(Z_Construct_UClass_ASamBaseChar, &ASamBaseChar::StaticClass, TEXT("/Script/SecretAgentManV3"), TEXT("ASamBaseChar"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASamBaseChar);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

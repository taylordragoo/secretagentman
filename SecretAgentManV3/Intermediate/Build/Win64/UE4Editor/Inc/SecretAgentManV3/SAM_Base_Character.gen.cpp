// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SecretAgentManV3/SAM_Base_Character.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSAM_Base_Character() {}
// Cross Module References
	SECRETAGENTMANV3_API UClass* Z_Construct_UClass_ASAM_Base_Character_NoRegister();
	SECRETAGENTMANV3_API UClass* Z_Construct_UClass_ASAM_Base_Character();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_SecretAgentManV3();
// End Cross Module References
	void ASAM_Base_Character::StaticRegisterNativesASAM_Base_Character()
	{
	}
	UClass* Z_Construct_UClass_ASAM_Base_Character_NoRegister()
	{
		return ASAM_Base_Character::StaticClass();
	}
	struct Z_Construct_UClass_ASAM_Base_Character_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASAM_Base_Character_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_SecretAgentManV3,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASAM_Base_Character_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "SAM_Base_Character.h" },
		{ "ModuleRelativePath", "SAM_Base_Character.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASAM_Base_Character_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASAM_Base_Character>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASAM_Base_Character_Statics::ClassParams = {
		&ASAM_Base_Character::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASAM_Base_Character_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASAM_Base_Character_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASAM_Base_Character()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASAM_Base_Character_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASAM_Base_Character, 2102280715);
	template<> SECRETAGENTMANV3_API UClass* StaticClass<ASAM_Base_Character>()
	{
		return ASAM_Base_Character::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASAM_Base_Character(Z_Construct_UClass_ASAM_Base_Character, &ASAM_Base_Character::StaticClass, TEXT("/Script/SecretAgentManV3"), TEXT("ASAM_Base_Character"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASAM_Base_Character);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
